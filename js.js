const formulario = document.getElementById("formulario");
const listaAlumnos = document.getElementById("lista-alumnos");
const nombresArray = [];
formulario.addEventListener("submit", (event) => {
  event.preventDefault();
  const nombres = document.getElementById("nombres").value;
  const apellidos = document.getElementById("apellidos").value;
  const curso = document.getElementById("curso").value;
  const alumno = {
    nombres,
    apellidos,
    curso,
  };
  agregarAlumno(alumno);
  mostrarListaAlumnos();
});
function agregarAlumno(alumno) {
  nombresArray.push(alumno); 
  console.log("Alumno agregado:", alumno.nombres);
}
function mostrarListaAlumnos() {
  listaAlumnos.innerHTML = ""; 
  for (const alumno of nombresArray) {
    const li = document.createElement("li");
    li.textContent = `${alumno.nombres} ${alumno.apellidos} - Curso: ${alumno.curso}`;
    listaAlumnos.appendChild(li);
  }
}
function cancelar() {
  document.getElementById("formulario").reset();
}
